# Adminka

Administrative panel for School owners and Courses creators.

## Roles

### School Owner

Eligable for Shool's information, [Courses](#) Catalog, [Students](#), [Payments](#).

### Course Creator

Manage Courses, Lessons and other Catalog's information.

## Functionality

Depicted in main menu.

### Overview

Generic feeds and dashboards.

### Analytica

### User Management

- List of creators, teachers, students.
- RBAC

### Geo

Regional settings and statistics.

### Orders

Orders management and payments.

### News

Feeds and alerts.

### General

General school settings.

### History 

List of changes and actions.

### Settings

Account settings.
